# -*- coding:utf-8 -*-
import init
from flask_cors import CORS
import faulthandler
import os
import threading
from utils.common.scheduler import scheduler

# 开启错误日志记录
faulthandler.enable()

app = init.create_app()


def run_scheduler():
    # 启动调度器
    scheduler.start()
    # 让子线程一直运行，直到主线程退出
    while True:
        pass


# r'/*' 是通配符，让本服务器所有的 URL 都允许跨域请求
CORS(app, resources=r'/*')
if __name__ == '__main__':
    # 创建新的线程并运行调度器
    scheduler_thread = threading.Thread(target=run_scheduler)
    scheduler_thread.start()
    os.environ["EXECJS_RUNTIME"] = "Node"
    app.run(host="0.0.0.0", port=5003)
