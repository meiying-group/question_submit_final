# -*- coding:utf-8 -*-
from flask_restful import Resource, fields, marshal_with, request
import view.common as view_common
import service.web_service as service
from exception.api_exception import APIException
import traceback
import execjs
from utils.common.init_app import app


class Health(Resource):
    """
    测试连接池大小
    """
    res_obj = {
        'code': fields.Integer,
        'message': fields.String,
        'result': fields.Raw
    }

    @marshal_with(res_obj)
    def get(self):
        app.logger.info('我打印了一个日志输出')
        from utils.common.db_connection import db
        from flask_sqlalchemy import __version__
        res = {'version': __version__, 'pool_size': db.engine.pool.size()}
        return view_common.success(res=res)


class TaskStop(Resource):
    """
        任务完成
        """
    res_obj = {
        'code': fields.Integer,
        'message': fields.String,
        'result': fields.Raw
    }

    @marshal_with(res_obj)
    def post(self):
        data = request.get_json()
        work_order_id_str = data.get("work_order_id")
        if work_order_id_str is None or work_order_id_str == "":
            return view_common.failed_10001(res="work_order_id is null or ''")
        work_order_id = int(work_order_id_str)
        res = service.task_stop(work_order_id)
        return view_common.success(res=res)


class TestExecjsEnv(Resource):
    """
    execjs的环境
    """
    res_obj = {
        'code': fields.Integer,
        'message': fields.String,
        'result': fields.Raw
    }

    @marshal_with(res_obj)
    def get(self):
        res = execjs.get().name
        return view_common.success(res=res)


class StartTaskWjx(Resource):
    """
    任务完成
    """
    res_obj = {
        'code': fields.Integer,
        'message': fields.String,
        'result': fields.Raw
    }

    @marshal_with(res_obj)
    def post(self):
        try:
            data = request.get_json()
            work_order_id = int(data.get("work_order_id"))  # 工单id
            send_count_arr = data.get("send_count_arr")  # 需要发送的id数组
            item_answer_start = int(data.get("item_answer_start"))  # 一行数据的答题结束时间
            item_answer_end = int(data.get("item_answer_end"))  # 一行数据的答题开始时间
            interval_start = int(data.get("interval_start"))  # 请求时间间隔 - 开始
            is_fast_order = int(data.get("is_fast_order"))  # 请求时间间隔
            interval_end = int(data.get("interval_end"))  # 请求时间间隔
            can_answer_start = data.get("can_answer_start")  # 开始运行时间
            can_answer_end = data.get("can_answer_end")  # 结束运行时间
            area_value_weight_dict = data.get("area_value_weight_dict")  # 发送区域的配置
            link = data.get("link")  # 发送的链接
        except Exception:
            traceback.print_exc()
            raise APIException(message="参数错误")
        res = service.start_task_wjx(work_order_id, link, send_count_arr, item_answer_start, item_answer_end,
                                     interval_start, interval_end, area_value_weight_dict, is_fast_order,
                                     can_answer_start, can_answer_end)
        return view_common.success(res=res)


class StartTask(Resource):
    """
    启动问卷任务（通用）
    """
    res_obj = {
        'code': fields.Integer,
        'message': fields.String,
        'result': fields.Raw
    }

    @marshal_with(res_obj)
    def post(self):
        try:
            data = request.get_json()
            work_order_id = int(data.get("work_order_id"))  # 工单id
            send_count_arr = data.get("send_count_arr")  # 需要发送的id数组
            item_answer_start = int(data.get("item_answer_start"))  # 一行数据的答题结束时间
            item_answer_end = int(data.get("item_answer_end"))  # 一行数据的答题开始时间
            interval_start = int(data.get("interval_start"))  # 请求时间间隔 - 开始
            is_fast_order = int(data.get("is_fast_order"))  # 请求时间间隔
            interval_end = int(data.get("interval_end"))  # 请求时间间隔
            can_answer_start = data.get("can_answer_start")  # 开始运行时间
            can_answer_end = data.get("can_answer_end")  # 结束运行时间
            area_value_weight_dict = data.get("area_value_weight_dict")  # 发送区域的配置
            link = data.get("link")  # 发送的链接
        except Exception:
            traceback.print_exc()
            raise APIException(message="参数错误")
        res = service.start_task(work_order_id, link, send_count_arr, item_answer_start, item_answer_end,
                                 interval_start, interval_end, area_value_weight_dict, is_fast_order,
                                 can_answer_start, can_answer_end)
        return view_common.success(res=res)


class StartTaskWjw(StartTask):
    """开始问卷网任务"""
    pass


class StartTaskTx(StartTask):
    """开始腾讯问卷任务"""
    pass


class StartTaskGg(StartTask):
    """开始谷歌问卷任务"""
    pass


class StartTaskQualtrics(StartTask):
    """开始Qualtrics问卷任务"""
    pass


class StartTaskCredamo(StartTask):
    """开始见数问卷任务"""
    pass
