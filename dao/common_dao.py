import model.model as ut
from utils.common.db_connection import db


def batch_insert(objs: []):
    # 批量提交
    db.session.add_all(objs)
    db.session.commit()


def add_commit(obj):
    db.session.add(obj)
    db.session.commit()
