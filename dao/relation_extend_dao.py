import model.model as ut
from utils.common.db_connection import db, JSON


def batch_insert(objs: [ut.RelationExtend]):
    # 批量提交
    
    db.session.add_all(objs)
    db.session.commit()


def search_by_work_order_id(work_order_id: int) -> ut.RelationExtend:
    """
    通过work_order_id查询相关性extend的数据 - 仅一条
    :param work_order_id:
    :return:
    """
    datas = db.session.query(ut.RelationExtend).filter_by(work_order_id=work_order_id).first()
    return datas


def modify_by_work_order_id(wo_id: int, relation_res) -> ut.RelationExtend:
    """
    修改数据内容
    :param wo_id:
    :param relation_res:
    :return:
    """
    
    item = db.session.query(ut.RelationExtend).filter_by(work_order_id=wo_id).first()
    if item:
        item.relation_res = relation_res
        db.session.add(item)
        db.session.commit()
    return item


def confirm_relation_res(wo_id: int) -> ut.RelationExtend:
    """
    确认分析结果
    :param wo_id: 工单id
    :return:
    """
    
    item = db.session.query(ut.RelationExtend).filter_by(work_order_id=wo_id).first()
    if item:
        item.confirmed_relation_res = item.relation_res
        db.session.commit()
    return item
