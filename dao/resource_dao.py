import model.model as ut
from utils.common.db_connection import db


def search_by_id(resource_id: int) -> ut.Resource:
    """
    通过主键id查询
    :param resource_id:
    :return:
    """
    return ut.Resource.query.get(resource_id).first()


def search_by_ids(resource_ids: [int]) -> [ut.Resource]:
    """
    通过主键id批量查询
    :param resource_ids:
    :return:
    """
    return ut.Resource.query.filter(ut.Resource.id.in_(resource_ids)).all()
