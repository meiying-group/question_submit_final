import model.model as ut
from utils.common.db_connection import db


def delete_by_id(id: int):
    """
    通过id删除尚待你
    :param id:
    :return:
    """
    # 查询符合条件的数据并删除
    data_to_delete = db.session.query(ut.Shop).filter_by(id=id).all()
    for data in data_to_delete:
        db.session.delete(data)
    db.session.commit()


def batch_insert(objs: [ut.Shop]):
    # 批量提交
    db.session.add_all(objs)
    db.session.commit()


def query_shops(page_num: int, page_size: int):
    query = db.session.query(ut.Shop)
    total_count = query.count()
    shops = query.offset(page_num).limit(page_size).all()
    return total_count, shops


def query_shops_all():
    """
    查询所有门店信息
    :return:
    """
    query = db.session.query(ut.Shop)
    total_count = query.count()
    shops = query.all()
    return total_count, shops
