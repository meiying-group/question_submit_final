import model.model as ut
from utils.common.db_connection import db


def batch_insert(objs: [ut.Strategy]):
    # 批量提交
    db.session.add_all(objs)
    db.session.commit()


def search_by_work_order_id(work_order_id: int) -> ut.Strategy:
    """
    通过work_order_id查询策略信息
    :param work_order_id:
    :return:
    """
    datas = db.session.query(ut.Strategy).filter_by(work_order_id=work_order_id).first()
    return datas


def modify_by_work_order_id(wo_id: int, strategy_weight_res) -> ut.Strategy:
    """
    修改数据内容
    :param wo_id:
    :param strategy_weight_res:
    :return:
    """
    item = db.session.query(ut.Strategy).filter_by(work_order_id=wo_id).first()
    if item:
        item.strategy_weight = strategy_weight_res
        db.session.commit()
    return item


def modify_programmes_by_work_order_id(wo_id: int, programmes) -> ut.Strategy:
    """
    修改数据内容
    :param wo_id:
    :param programmes:
    :return:
    """
    item = db.session.query(ut.Strategy).filter_by(work_order_id=wo_id).first()
    if item:
        item.strategy_programme = programmes
        db.session.commit()
    return item


def modify_relation_config_by_work_order_id(wo_id: int, relation_config) -> ut.Strategy:
    """
    修改数据内容
    :param wo_id:
    :param relation_config:
    :return:
    """
    item = db.session.query(ut.Strategy).filter_by(work_order_id=wo_id).first()
    if item:
        item.relation_config = relation_config
        db.session.add(item)
        db.session.commit()
    return item


def modify_submit_config_by_work_order_id(wo_id: int, submit_config) -> ut.Strategy:
    """
    修改数据内容
    :param wo_id:
    :param submit_config: 提交的配置参数
    :return:
    """
    item = db.session.query(ut.Strategy).filter_by(work_order_id=wo_id).first()
    if item:
        item.submit_config = submit_config
        db.session.add(item)
        db.session.commit()
    return item
