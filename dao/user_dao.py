import model.model as ut
from utils.common.db_connection import db


def query_user_all():
    """
    查询所有用户
    :return:
    """
    query = db.session.query(ut.User)
    total_count = query.count()
    users = query.all()
    return total_count, users


def query_user_by_ids(ids):
    cur_list = db.session.query(ut.User).filter(ut.User.id.in_(ids)).all()
    return cur_list


def query_user_by_user_name(cur_user_name: str) -> ut.User:
    return db.session.query(ut.User).filter_by(user_name=cur_user_name).first()


def insert_user(name_zh: str, user_name: str, password: str):
    """
    新增用户
    :param name_zh:
    :param user_name:
    :param password:
    :return:
    """
    obj = ut.User()
    obj.name_zh = name_zh
    obj.user_name = user_name
    obj.hash_password(password=password)
    db.session.add(obj)
    db.session.commit()
