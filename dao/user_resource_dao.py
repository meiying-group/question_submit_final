import model.model as ut
from utils.common.db_connection import db


def search_by_user_id(user_id: int) -> [ut.UserResource]:
    """
    通过user_id查询对应的资源ID
    :param user_id:
    :return:
    """
    # 查询符合条件的数据并删除
    return db.session.query(ut.UserResource).filter_by(user_id=user_id).all()


def search_by_resource_ids2(resource_ids: []):
    return ut.UserResource.query.join(ut.User, ut.UserResource.user_id == ut.User.id, isouter=True).filter(
        ut.UserResource.resource_id.in_(resource_ids)).all()


def search_by_resource_ids(resource_ids: []) -> [ut.UserResource]:
    """
    通过资源ID查询
    :param resource_ids:
    :return:
    """
    # 查询符合条件的数据并删除
    return ut.UserResource.query.filter(ut.UserResource.resource_id.in_(resource_ids)).all()
