import model.model as ut
from utils.common.db_connection import db


def batch_insert(objs: [ut.WorkOrderExtend]):
    # 批量提交
    db.session.add_all(objs)
    db.session.commit()


def delete_result_by_word_order_id(work_order_id: int):
    """
    通过work_order_id删除工单
    :param work_order_id:
    :return:
    """
    # 查询符合条件的数据并删除
    data_to_delete = db.session.query(ut.WorkOrderExtend).filter_by(work_order_id=work_order_id).all()
    if not data_to_delete:
        return
    for data in data_to_delete:
        db.session.delete(data)
    db.session.commit()


def search_by_work_order_id(work_order_id: int) -> ut.WorkOrderExtend:
    """
    通过work_order_id查询标题信息
    :param work_order_id:
    :return:
    """
    datas = db.session.query(ut.WorkOrderExtend).filter_by(work_order_id=work_order_id).first()
    return datas
