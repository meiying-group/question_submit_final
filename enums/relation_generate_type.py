from enum import Enum


class RelationGenerateType(Enum):
    NORMALITY = "NORMALITY"  # 正态
    SKEWNESS = "SKEWNESS"  # 偏态
