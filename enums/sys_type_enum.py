from enum import Enum


class SysTypeEnum(Enum):
    # SINGLE = "单选题"
    # SINGLE_SELECT = "单选题(下拉选择)"
    # MULTIPLE = "多选题"
    # SORTED = "排序题"
    # SCALE = "量表题"
    # MATRIX = "矩阵题"
    # MULTIPLE_MATRIX = "矩阵多选题"
    # SINGLE_SLIDE = "单项滑动题"
    # SLIDE = "滑动题"
    # RATE = "比重题"
    # TEXT = "填空题"
    # AREA = "地区题"
    # DATE = "日期题"
    # MULTIPLE_TEXT = "多项填空"
    # MATRIX_TEXT = "矩阵填空"

    SINGLE = "SINGLE"
    SINGLE_SELECT = "SINGLE_SELECT"
    MULTIPLE = "MULTIPLE"
    SORTED = "SORTED"
    SCALE = "SCALE"
    MATRIX = "MATRIX"
    MULTIPLE_MATRIX = "MULTIPLE_MATRIX"
    SINGLE_SLIDE = "SINGLE_SLIDE"
    SLIDE = "SLIDE"
    RATE = "RATE"
    TEXT = "TEXT"
    AREA = "AREA"
    DATE = "DATE"
    MULTIPLE_TEXT = "MULTIPLE_TEXT"
    MATRIX_TEXT = "MATRIX_TEXT"
    DESCRIPTION = "DESCRIPTION"  # 描述题-仅做展示
    CASCADE_DROPDOWN = "CASCADE_DROPDOWN"  # 级联下拉题
