from enum import Enum


class TopicViewStatusEnum(Enum):
    ALL = 0  # 全部
    SUBMITTED = 1  # 已提交
    UN_SUBMIT = 2  # 未提交
    FAILED_SUBMIT = 3  # 提交失败
