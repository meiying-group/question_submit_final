from enum import Enum


class WorkOrderStatusEnum(Enum):
    UNACCEPTED_ORDERS = 0  # 未接单
    ACCEPTED_ORDERS = 1  # 已接单
    BASE_DATA = 2  # 已生成基础数据
    RELATION_DATA = 3  # 已相关性数据
    WAITED_SEND = 4  # 待发送
    SENDING = 5  # 发送中
    FINISHED = 6  # 已完成
    CHARGEBACK = 7  # 已退单
    SEND_ERROR = 8  # 发送异常
