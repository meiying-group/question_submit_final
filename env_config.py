import os

"""
开发环境配置
"""
dev_config = {
    'DEBUG': False,
    'BIND': '0.0.0.0:5002',
    # 'SQLALCHEMY_DATABASE_URI': 'mysql://root:961110lhw@localhost:3306/logo',
    'SQLALCHEMY_DATABASE_URI': 'mysql+pymysql://root:Yydj_2023f@rm-2vc17il54x88franjho.mysql.cn-chengdu.rds.aliyuncs'
                               '.com:3306/analysis_test',
    'SQLALCHEMY_ENGINE_OPTIONS': {
        'pool_size': 200,
        'max_overflow': 4000,
        # 'pool_timeout': 30, # 指定了从连接池获取连接的超时时间（以秒为单位）。如果在指定的时间内无法获取到连接，将会抛出 sqlalchemy.exc.TimeoutError 异常。默认值为 10。
        # 'pool_recycle': 3600, # 指定了连接在被归还到连接池前的最大生存时间（以秒为单位）。超过这个时间的连接将被关闭并重新创建新的连接。默认值为 -1，表示禁用连接的回收。
        # 'pool_pre_ping': True # 指定了在每次从连接池获取连接时，是否自动执行一个轻量级的 Ping 操作来验证连接的健康状态。默认值为 False。
    },
    # 'WJX_7_CHECK_SERVER': 'http://47.109.67.202:5004',  # 服务器
    # 'WJX_7_CHECK_SERVER': 'http://127.0.0.1:5004',  # 本地
    'WJX_7_CHECK_SERVER': 'http://0.0.0.0:5004',  # 外部_本地1
    # 'image_save_path': 'C:/Users/Administrator/Desktop',
    # 'CALLBACK_URL': 'https://3s93179e88.imdo.co/api/v1/webhook'
    'REDIS_CONFIG': {
        # 'host': '192.168.16.178',
        'host': 'r-2vcus8exwk11f8jhhppd.redis.cn-chengdu.rds.aliyuncs.com',
        'port': 6379,
        'password': 'Yydj@!#qaz2023',
        'db': '0',
        'expire': 60 * 60,
    },
}

"""
线上环境配置
"""
prod_config = {
    'DEBUG': False,
    'BIND': '0.0.0.0:5003',
    # 'SQLALCHEMY_DATABASE_URI': 'mysql+pymysql://root:Yydj_2023f@rm-2vc17il54x88franjho.mysql.cn-chengdu.rds.aliyuncs'
    #                            '.com:3306/analysis',
    'SQLALCHEMY_DATABASE_URI': 'mysql+pymysql://root:Yydj_2023f@rm-2vc17il54x88franj.mysql.cn-chengdu.rds.aliyuncs'
                               '.com:3306/analysis',
    'SQLALCHEMY_ENGINE_OPTIONS': {
        'pool_size': 200,
        'max_overflow': 4000,
        'pool_timeout': 30,
    },
    'WJX_7_CHECK_SERVER': 'http://0.0.0.0:5004',
    # 'image_save_path': '/home/files/logo/upload',
    # 'CALLBACK_URL': 'http://47.109.67.202:5100/api/v1/webhook'
    'REDIS_CONFIG': {
        # 'host': 'r-2vcus8exwk11f8jhhp.redis.cn-chengdu.rds.aliyuncs.com',  # 专有网络
        'host': 'r-2vcus8exwk11f8jhhppd.redis.cn-chengdu.rds.aliyuncs.com',  # 公网
        'port': 6379,
        'password': 'Yydj@!#qaz2023',
        'db': '4',
        'expire': 60 * 60,
    },
}

"""
    配置文件映射
"""
CONFIG_NAME_MAPPER = {
    'local': dev_config,
    'dev': dev_config,
    'prod': prod_config,
    # 'test': dev_config
}


def config_mapping() -> dict:
    env_flask_config_name = os.getenv('ANALYSIS_SCHEDULE_CONFIG')
    config_mapper_name = env_flask_config_name or 'local'
    return CONFIG_NAME_MAPPER[config_mapper_name]
