from werkzeug.exceptions import HTTPException


class APIException(HTTPException):
    def __init__(self, message, code=500):
        super(HTTPException, self).__init__()
        self.raw_message = message
        # self.error_code = error_code
        self.code = code
        self.message = message

    def to_dict(self):
        result = {
            # "error_code": self.error_code,
            "result": {},
            "code": self.code,
            "message": self.message,
        }
        return result
