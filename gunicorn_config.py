import multiprocessing
import env_config
import os

# 设置环境变量
os.environ['ANALYSIS_SCHEDULE_CONFIG'] = 'prod'
# from gevent import monkey
#
# preload_app = False
# monkey.patch_all()
# 是否开启debug模式
debug = False
# 访问地址
# bind = "0.0.0.0:5000"
bind = env_config.config_mapping().get("BIND")  # 获取绑定的端口号
# 工作进程数
workers = multiprocessing.cpu_count() * 2 + 1
# # 工作线程数
threads = multiprocessing.cpu_count() * 2
# # 设置协程模式
# worker_class = "gevent"
worker_class = "gthread"
# worker_class = "egg:meinheld#gunicorn_worker"
# # 最大客户端并发数量，默认情况下这个值为1000。此设置将影响gevent和eventlet工作模式
# worker_connections = 1200
# 超时时间
timeout = 600
# 输出日志级别
loglevel = 'info'
# 存放日志路径
pidfile = "./../log/gunicorn.pid"
# # 存放日志路径
accesslog = "./../log/access.log"
access_log_format = '%(h) -  %(t)s - %(u)s - %(s)s %(H)s'
# # 存放日志路径
errorlog = "./../log/analysis.log"
# gunicorn + apscheduler场景下，解决多worker运行定时任务重复执行的问题
preload_app = True
