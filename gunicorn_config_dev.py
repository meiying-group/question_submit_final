import multiprocessing
import env_config

# from gevent import monkey
#
# monkey.patch_all()
# 是否开启debug模式
debug = False
# 访问地址, dev默认访问5151
# bind = "0.0.0.0:5002"
bind = env_config.config_mapping().get("BIND")  # 获取绑定的端口号
# 工作进程数
workers = 4
# # 工作线程数
threads = 2
# # 设置协程模式
# worker_class = "gevent"
worker_class = "gthread"
# worker_class = "egg:meinheld#gunicorn_worker"
# # 最大客户端并发数量，默认情况下这个值为1000。此设置将影响gevent和eventlet工作模式
# worker_connections = 1200
# 超时时间
timeout = 600
# 输出日志级别
loglevel = 'info'
# 存放日志路径
pidfile = "./../log_dev/gunicorn.pid"
# # 存放日志路径
accesslog = "./../log_dev/access.log"
access_log_format = '%(h) -  %(t)s - %(u)s - %(s)s %(H)s'
# # 存放日志路径
errorlog = "./../log_dev/analysis.log"
# gunicorn + apscheduler场景下，解决多worker运行定时任务重复执行的问题
# preload_app = True
# 最大请求数量：超过这个数量会重启当前worker
max_requests = 20
# 接收到restart信号后，worker可以在graceful_timeout时间内，继续处理完当前requests
graceful_timeout = 200
# max_requests的最大抖动值
max_requests_jitter = 1
