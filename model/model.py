from utils.common.db_connection import db, JSON
from dataclasses import dataclass, asdict, field


# 定义ORM
# @dataclass
class WorkOrder(db.Model):
    __tablename__ = 'work_order'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    ww = db.Column(db.String(128), unique=False)
    order_id = db.Column(db.String(128), unique=True)
    shop_id = db.Column(db.Integer, unique=False)
    make_user_id = db.Column(db.Integer, unique=False)
    get_user_id = db.Column(db.Integer, unique=False)
    cost = db.Column(db.String(128), unique=False)
    total_size = db.Column(db.String(128), unique=False)
    link = db.Column(db.String(128), unique=False)
    priority = db.Column(db.Integer, unique=False)  # 优先级
    bz = db.Column(db.String(128), unique=False)  # 备注
    status = db.Column(db.Integer, unique=False)  # 状态
    feedback = db.Column(db.String(255), unique=False)  # 反馈
    oper_type = db.Column(db.Integer, unique=False)  # 操作类型
    plan_finished_time = db.Column(db.Integer, unique=False)  # 计划完成时间
    ordered_time = db.Column(db.Integer, unique=False)  # 下单时间
    area_note = db.Column(db.String(512), unique=False)  # 地区备注


class WorkOrderExtend(db.Model):
    __tablename__ = 'work_order_extend'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)  # 主键
    work_order_id = db.Column(db.Integer, unique=True)  # 工单ID
    titles = db.Column(JSON)  # 虚拟标题的下标
    title_maps = db.Column(JSON)  # 真实标题与虚拟标题的下标映射


class Shop(db.Model):
    __tablename__ = 'shop'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    code = db.Column(db.String(128), unique=True)
    shop_name = db.Column(db.String(128), unique=False)


# @dataclass
class Topic(db.Model):
    __tablename__ = 'topic'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)  # 用户id
    data = db.Column(db.String(512), unique=False)  # 基础数据
    work_order_id = db.Column(db.Integer, unique=False)  # 工单ID
    submitted = db.Column(db.Integer, unique=False)  # 0:未提交,1:提交中,2:已提交
    submit_start_time = db.Column(db.Integer, unique=False)  # 提交的开始时间
    submit_end_time = db.Column(db.Integer, unique=False)  # 提交的结束时间
    request_res = db.Column(db.String(512), unique=False)  # 请求返回的结果
    request_ip = db.Column(db.String(512), unique=False)  # 请求使用的ip地址


class Relation(db.Model):
    __tablename__ = 'relation'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)  # 用户id
    relation_data = db.Column(db.String(512), unique=False)  # 关系相关的数据
    relation_index = db.Column(db.String(512), unique=False)  # 数据的索引
    work_order_id = db.Column(db.Integer, unique=False)  # 工单ID


class RelationExtend(db.Model):
    __tablename__ = 'relation_extend'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)  # 主键id
    work_order_id = db.Column(db.Integer, unique=True)  # 工单ID
    confirmed_relation_res = db.Column(JSON)  # 已确认的相关性结果
    relation_res = db.Column(JSON)  # 未确认的相关性结果


class Strategy(db.Model):
    __tablename__ = 'strategy'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)  # 主键
    work_order_id = db.Column(db.Integer, unique=True)  # 工单ID
    strategy_weight = db.Column(JSON)  # 权重
    strategy_programme = db.Column(JSON)  # 方案
    relation_config = db.Column(JSON)  # 相关性配置
    submit_config = db.Column(JSON)  # 提交配置


class Resource(db.Model):
    __tablename__ = 'resource'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)  # 主键
    resource_code = db.Column(db.String(255), unique=True)  # 资源编码
    resource_name = db.Column(db.String(255), unique=False)  # 资源名称


class UserResource(db.Model):
    __tablename__ = 'user_resource'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)  # 主键
    user_id = db.Column(db.Integer, unique=True)  # 用户id
    resource_id = db.Column(db.Integer, unique=True)  # 资源id
