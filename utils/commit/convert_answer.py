# 转换答案

class Convert2SubmitData(object):
    def __init__(self, data, title_maps):
        self.data = data
        self.title_maps = title_maps

    # def get_submit_data(self):
    #     datas = self.data
    #     title_maps = self.title_maps
    #     for i in title_maps:


if __name__ == '__main__':
    # 1$3}2$3^x}3$1|2|3|4}4$1|2|3|4^x}5$1!40^2!23}6$1,2,3}7$1,2,3^x}8$1!35^2!65}9$北京-北京市-东城区}10$1}11$5}12$xy}13$2023-08-28}14$1!4,2!4,3!5,4!4,5!4}15$1!5,2!5,3!4,4!5,5!5}16$1!26^2!21^3!36^4!23^5!48}17$11
    # 1$3}2$3^x}3$1|2|3|4}4$1|2|3|4^x}5$1!71^2!54}6$2,1,3}7$2,1,3}8$1!90^2!10}9$北京-北京市-东城区}10$2}11$2}12$填空1}13$2023-08-27}14$1!5,2!4,3!5,4!5,5!3}15$1!5,2!5,3!5,4!3,5!5}16$1!40^2!99^3!63^4!63^5!51}17$2}

    # 1$1}2$1^x}3$1|2|3|4}4$1|4}5$1!9^2!21}6$1,3,2}7$1,2,3}8$1!25^2!75}9$北京-北京市-东城区}10$1}11$1}12$ll}13$2023-08-20}14$1!5,2!5,3!5,4!5,5!5}15$1!2,2!2,3!2,4!2,5!2}16$1!22^2!39^3!56^4!77^5!96}17$7
    db_title_arr = [{"type": 3, "title_id": "1", "data_index": [{"index": 0, "title": "1. 标题*", "option_count": 3}]},
                    {"type": 3, "title_id": "2", "data_index": [{"index": 1, "title": "2. 标题*", "option_count": 3}]},
                    {"type": 4, "title_id": "3",
                     "data_index": [{"index": 2, "title": "3. 标题*【多选题】(1)", "option_count": 1},
                                    {"index": 3, "title": "3. 标题*【多选题】(2)", "option_count": 1},
                                    {"index": 4, "title": "3. 标题*【多选题】(3)", "option_count": 1},
                                    {"index": 5, "title": "3. 标题*【多选题】(4)", "option_count": 1}]},
                    {"type": 4, "title_id": "4",
                     "data_index": [{"index": 6, "title": "4. 标题*【多选题】(1)", "option_count": 1},
                                    {"index": 7, "title": "4. 标题*【多选题】(2)", "option_count": 1},
                                    {"index": 8, "title": "4. 标题*【多选题】(3)", "option_count": 1},
                                    {"index": 9, "title": "4. 标题*【多选题】(4)", "option_count": 1}]},
                    {"type": 9, "title_id": "5", "data_index": [{"index": 10, "title": "5. 标题*(外观)", "option_count": 1},
                                                                {"index": 11, "title": "5. 标题*(功能)",
                                                                 "option_count": 1}]},
                    {"type": 11, "title_id": "6",
                     "data_index": [{"index": 12, "title": "6. 标题*【排序题】(选项11)", "option_count": 3},
                                    {"index": 13, "title": "6. 标题*【排序题】(选项12)", "option_count": 3},
                                    {"index": 14, "title": "6. 标题*【排序题】(选项13)", "option_count": 3}]},
                    {"type": 11, "title_id": "7",
                     "data_index": [{"index": 15, "title": "7. 标题*【排序题】(选项11)", "option_count": 3},
                                    {"index": 16, "title": "7. 标题*【排序题】(选项12)", "option_count": 3},
                                    {"index": 17, "title": "7. 标题*【排序题】(选项13)", "option_count": 3}]},
                    {"type": 12, "title_id": "8",
                     "data_index": [{"index": 18, "title": "8. 标题*(外观)", "option_count": 1},
                                    {"index": 19, "title": "8. 标题*(性能)", "option_count": 1}]},
                    {"type": 1, "title_id": "9",
                     "data_index": [{"index": 20, "title": "9. 请选择省份城市与地区:*", "option_count": 1}]},
                    {"type": 7, "title_id": "10", "data_index": [{"index": 21, "title": "10. 标题*", "option_count": 3}]},
                    {"type": 5, "title_id": "11", "data_index": [{"index": 22, "title": "11. 标题*", "option_count": 5}]},
                    {"type": 1, "title_id": "12", "data_index": [{"index": 23, "title": "12. 标题*", "option_count": 1}]},
                    {"type": 1, "title_id": "13",
                     "data_index": [{"index": 24, "title": "13. 请输入您的出生日期：*", "option_count": 1}]},
                    {"type": 6, "title_id": "14",
                     "data_index": [{"index": 25, "title": "14. 标题*(1)", "option_count": 5},
                                    {"index": 26, "title": "14. 标题*(2)", "option_count": 5},
                                    {"index": 27, "title": "14. 标题*(3)", "option_count": 5},
                                    {"index": 28, "title": "14. 标题*(4)", "option_count": 5},
                                    {"index": 29, "title": "14. 标题*(5)", "option_count": 5}]},
                    {"type": 6, "title_id": "15",
                     "data_index": [{"index": 30, "title": "15. 标题*(1)", "option_count": 5},
                                    {"index": 31, "title": "15. 标题*(2)", "option_count": 5},
                                    {"index": 32, "title": "15. 标题*(3)", "option_count": 5},
                                    {"index": 33, "title": "15. 标题*(4)", "option_count": 5},
                                    {"index": 34, "title": "15. 标题*(5)", "option_count": 5}]},
                    {"type": 9, "title_id": "16",
                     "data_index": [{"index": 35, "title": "16. 标题*(1)", "option_count": 1},
                                    {"index": 36, "title": "16. 标题*(2)", "option_count": 1},
                                    {"index": 37, "title": "16. 标题*(3)", "option_count": 1},
                                    {"index": 38, "title": "16. 标题*(4)", "option_count": 1},
                                    {"index": 39, "title": "16. 标题*(5)", "option_count": 1}]},
                    {"type": 5, "title_id": "17",
                     "data_index": [{"index": 40, "title": "17. 您向朋友或同事推荐我们的可能性有多大？*", "option_count": 11}]}]
    # db_res_arr = [3, "3^x", 1, 2, 3, 4, 1, 2, 3, '4^x', 71, 54, 2, 1, 3, 2, 1, 3, 90, 10, "北京-北京市-东城区", "2", "2",
    #               "填空1", "2023-08-27", 5, 4, 5, 5, 3, 5, 5, 5, 3, 5, 40, 99, 63, 63, 51, "2"]
    db_res_arr = [3, "3^填空答案", 0, 1, 1, 1, 0, 1, 0, 0, 27, 72, 1, 3, 2, 2, 1, 3, 17, 42, "填空10", "2", "3", "填空10",
                  "填空10", 5, 5, 3, 5, 4, 5, 5, 4, 4, 5, 26, 18, 8, 56, 100, "11"]
    res = ''
    for index in range(len(db_title_arr)):
        i = db_title_arr[index]
        data_index = i['data_index']
        indexes = [i['index'] for i in data_index]
        s = i['title_id'] + '$'
        if len(indexes) > 1:
            if i['type'] == 4:
                for j in range(len(indexes)):
                    if j == len(indexes) - 1:
                        s += str(db_res_arr[indexes[j]])
                    else:
                        s += str(db_res_arr[indexes[j]]) + '|'
            elif i['type'] == 6:
                for j in range(len(indexes)):
                    if j == len(indexes) - 1:
                        s += str(j + 1) + '!' + str(db_res_arr[indexes[j]])
                    else:
                        s += str(j + 1) + '!' + str(db_res_arr[indexes[j]]) + ','
            elif i['type'] == 9 or i['type'] == 12:
                for j in range(len(indexes)):
                    if j == len(indexes) - 1:
                        s += str(j + 1) + '!' + str(db_res_arr[indexes[j]])
                    else:
                        s += str(j + 1) + '!' + str(db_res_arr[indexes[j]]) + '^'
            elif i['type'] == 11:
                for j in range(len(indexes)):
                    if j == len(indexes) - 1:
                        s += str(db_res_arr[indexes[j]])
                    else:
                        s += str(db_res_arr[indexes[j]]) + ','
            else:
                for j in range(len(indexes)):
                    s += str(db_res_arr[indexes[j]])
        else:
            s += str(db_res_arr[indexes[0]])
        if index < len(db_title_arr) - 1:
            s += '}'
        res += s
    print()
    print(res)
    exit()
