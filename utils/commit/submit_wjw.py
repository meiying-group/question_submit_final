from enums.sys_type_enum import SysTypeEnum


def convert_data_to_submit_data(db_title_arr, db_res_arr) -> str:
    """
    将数据库中的数据转换为需要提交的结果数据
    :param db_title_arr: 题目与数据库结果的映射关系数组
    :param db_res_arr: 需要提交的答案数组
    :return:
    """
    res = dict()
    for index in range(len(db_title_arr)):
        i = db_title_arr[index]
        indexes = i['data_index']
        cur_type = i['sys_type']
        title_id = i['title_id']
        if len(indexes) > 1:
            if cur_type == SysTypeEnum.SINGLE.value or cur_type == SysTypeEnum.SINGLE_SELECT.value:  # 单选题/下拉单选题
                # 数据格式："657c0a016ef13c25babf5513":["657c0a016ef13c25babf5512","天空答案"]
                single_options = i['options']
                single_db_values = str(db_res_arr[indexes[0]]).split('^')
                if single_db_values:
                    cur_value = single_db_values[0]
                    if '-3' == cur_value:  # -3直接跳过这条数据
                        continue
                    cur_text = None
                    if len(single_db_values) == 2:
                        cur_text = single_db_values[1]
                    for single_option in single_options:
                        if str(single_option['value']) == cur_value:  # 所有数据转换为字符串进行比较
                            v = [single_option['real_value']]
                            if cur_text:
                                v.append(v)
                            res[title_id] = v
                            break  # 找到匹配的选项后跳出子循环
            elif cur_type == SysTypeEnum.MULTIPLE.value:  # 多选题
                # 数据格式："657c0a0478067d8f02d579a4":[["657c0a0478067d8f02d579a1"],["657c0a0478067d8f02d579a2"]]
                options = i['options']
                item_title_res = []
                for option in options:
                    option_real_value = option['real_value']
                    db_value = db_res_arr[option['data_index']]
                    mu_arr = str(db_value).split('^')
                    if not mu_arr or '1' != mu_arr[0]:
                        continue
                    option_res = [option_real_value]
                    if len(mu_arr) == 2:
                        option_res.append(mu_arr[1])
                    item_title_res.append(option_res)
                if item_title_res:
                    res[title_id] = item_title_res
                # single_options = i['options']
                # value_dict = dict()
                # for option in single_options:
                #     value_dict[option['value']] = option['real_value']
                # value_arr = []
                # for j in range(len(indexes)):
                #     mu_arr = str(db_res_arr[indexes[j]]).split('^')
                #     if mu_arr:
                #         if str(mu_arr[0]) != '1':  # 如果不是1，则代表没有选择这个选项
                #             continue
                #         cur_value = str(j + 1)  # 自定义的选项值
                #         item_value_arr = [value_dict[cur_value]]
                #         if len(mu_arr) == 2:
                #             item_value_arr.append(mu_arr[1])
                #         value_arr.append(item_value_arr)
                # if value_arr:
                #     res[title_id] = value_arr
            elif cur_type == SysTypeEnum.SCALE.value:  # 单选量表题
                single_options = i['options']
                single_db_values = str(db_res_arr[indexes[0]]).split('^')
                if single_db_values:
                    if '-3' == single_db_values:  # -3直接跳过这条数据
                        continue
                    cur_value = single_db_values[0]
                    cur_text = None
                    if len(single_db_values) == 2:
                        cur_text = single_db_values[1]
                    for single_option in single_options:
                        if str(single_option['value']) == cur_value:  # 所有数据转换为字符串进行比较
                            v_dict = dict()
                            v = [cur_value]
                            if cur_text:
                                v.append(v)
                            v_dict[single_option['real_value']] = v
                            res[title_id] = v_dict
                            break  # 找到匹配的选项后跳出子循环
            elif cur_type == SysTypeEnum.TEXT.value:  # 填空题
                single_options = i['options']
                db_value = db_res_arr[indexes[0]]
                if '-3' == db_value:  # -3直接跳过这条数据
                    continue
                v_dict = dict()
                for single_option in single_options:
                    real_value = single_option['real_value']
                    real_value = real_value + '_open'
                    v_dict[real_value] = db_value
                    res[title_id] = v_dict
            elif cur_type == SysTypeEnum.MATRIX.value:  # 单选矩阵题
                children = i['children']
                v_dict = dict()  # 这个量表题的所有答案
                children_dict = dict()  # 这个矩阵题的索引和的对应的选项值
                for child in children:
                    options = child['options']
                    value_dict = dict()
                    for option in options:
                        value_dict[str(option['value'])] = option['real_value']
                    children_dict[child['data_index']] = value_dict
                for cur_index in indexes:
                    child_dict_value = children_dict[cur_index]
                    single_db_values = str(db_res_arr[cur_index]).split('^')
                    if single_db_values:
                        cur_value = single_db_values[0]  # 数字1，2，3，4等等
                        if '-3' == cur_value:  # -3直接跳过这条数据
                            continue
                        cur_uuid = child_dict_value[cur_value]
                        v_dict_value_arr = [cur_value]  # 每一个内部量表题的数组答案
                        if len(single_db_values) == 2:
                            v_dict_value_arr.append(single_db_values[1])
                        v_dict[cur_uuid] = v_dict_value_arr
                res[title_id] = v_dict
            elif cur_type == SysTypeEnum.CASCADE_DROPDOWN.value:  # 级联下拉题
                options = i['options']
                v_dict = dict()
                for option in options:
                    option_key = option['real_value'] + '_open'
                    option_value = db_res_arr[option['data_index']]
                    if '-3' == str(option_value):
                        continue
                    v_dict[option_key] = option_value
                if v_dict:
                    res[title_id] = v_dict
            elif cur_type == SysTypeEnum.SLIDE.value or cur_type == SysTypeEnum.RATE.value:
                cur_s = ''
                for j in range(len(indexes)):
                    if cur_s != '':
                        cur_s += '^'
                    cur_s += str(j + 1) + '!' + str(db_res_arr[indexes[j]])
                    # if j > len(indexes) - 1:
                    #     s += '^'
                s += cur_s
            elif cur_type == SysTypeEnum.SORTED.value:  # 排序题
                for j in range(len(indexes)):
                    s += str(db_res_arr[indexes[j]])
                    if j < len(indexes) - 1:
                        s += ','
            elif cur_type == SysTypeEnum.MULTIPLE_TEXT.value:  # 多项填空
                for j in range(len(indexes)):
                    s += str(db_res_arr[indexes[j]])
                    if j < len(indexes) - 1:
                        s += '^'
            elif cur_type == SysTypeEnum.MULTIPLE_MATRIX.value:  # 矩阵多选
                children = i['children']
                multiple_s = ''
                for child_index in range(len(children)):
                    child = children[child_index]
                    child_options = child['options']
                    if multiple_s != '':
                        multiple_s += ','
                    multiple_s += str(child_index + 1) + '!'
                    cur_s = ''
                    for j in range(len(child_options)):
                        mu_arr = str(db_res_arr[child_options[j]['data_index']]).split('^')
                        if mu_arr and mu_arr[0] == '1':  # 如果为1，表示需要提交这个选项
                            if cur_s != '':
                                cur_s += ';'
                            cur_s += str(j + 1)
                            if len(mu_arr) >= 2:
                                cur_s += '^' + str(mu_arr[1])
                    multiple_s += cur_s
                s += multiple_s
            elif cur_type == SysTypeEnum.MATRIX_TEXT.value:  # 矩阵填空
                cur_s = ''
                for j in range(len(indexes)):
                    if cur_s != '':
                        cur_s += '^'
                    cur_s += str(j + 1) + '!' + str(db_res_arr[indexes[j]])
                s += cur_s
            else:
                for j in range(len(indexes)):
                    s += str(db_res_arr[indexes[j]])
        else:
            s += str(db_res_arr[indexes[0]])
        if index < len(db_title_arr) - 1:
            s += '}'
        res += s
    return res
