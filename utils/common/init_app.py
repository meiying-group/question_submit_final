from flask import Flask
import datetime
import log_handler
import logging

app = Flask(__name__)
# 添加日志配置
app.logger.addHandler(log_handler.getLogHandler())
app.logger.setLevel(logging.DEBUG)
# 激活上下文
ctx = app.app_context()
ctx.push()

# app.config["JWT_SECRET_KEY"] = "super-secret"  # 设置 jwt 的秘钥
# app.config['JWT_ACCESS_TOKEN_EXPIRES'] = datetime.timedelta(days=2)  # 过期时间设置为2天
