from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.jobstores.redis import RedisJobStore
from apscheduler.executors.pool import ThreadPoolExecutor, ProcessPoolExecutor
import atexit
import env_config

# 执行器（executors）
executors = {
    'default': ThreadPoolExecutor(3000),
    # 'processpool': ProcessPoolExecutor(3000)
}
env_mp = env_config.config_mapping()
redis_config = env_mp.get('REDIS_CONFIG')  # 获取redis的配置
redis_host = redis_config.get('host')
redis_port = redis_config.get('port')
redis_password = redis_config.get('password')
redis_db = redis_config.get('db')
# redis_default_expire = redis_config.get('expire')  # 默认过期时间 - 任务队列不需要

jobstore = RedisJobStore(host=redis_host, port=redis_port, db=redis_db, password=redis_password)
job_defaults = {
    # 最近多久时间内允许存在的任务数
    'misfire_grace_time': 60 * 60 * 1,
    # 该定时任务允许最大的实例个数
    'max_instances': 10,
    # 是否运行一次最新的任务，当多个任务堆积时
    'coalesce': False,
    # 默认值的设置很科学啊
}
scheduler = BackgroundScheduler(executors=executors, job_defaults=job_defaults)
scheduler.add_jobstore(jobstore)
# scheduler.add_executor()
# 启用调度器
# scheduler.start()
# 在应用关闭时关闭调度器
atexit.register(lambda: scheduler.shutdown())
