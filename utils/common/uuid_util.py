#! /usr/bin/python
# -* - coding: UTF-8 -* -
import os
from uuid import UUID


def uuid4():
    """生成随机 UUID."""
    return UUID(bytes=os.urandom(16), version=4)


if __name__ == '__main__':
    print(str(uuid4()))
    pass
