from curl_cffi import requests

global_session = requests.Session()

# IP_DAY30_COUNT_6W_NO = '20231223297119471348'  # 优质池-30天-6w个
# IP_DAY30_COUNT_6W_SECRET = '4om51mo0dkbruo'  # 优质池-30天-6w个 - secret
IP_DAY30_COUNT_6W_NO = '20240404736205823993'  # 优质池-30天-6w个
IP_DAY30_COUNT_6W_SECRET = 'uabo66k80aqvlu8'  # 优质池-30天-6w个 - secret

IP_DAY30_COUNT_3W_NO = '20231123102211135097'  # 优质池-30天-3w个
IP_DAY30_COUNT_3W_SECRET = 'pbog2f6rme1cft'  # 优质池-30天-3w个 - secret

IP_DAY30_COUNT_1W_NO = '20231024368102845422'  # 优质-30天-上限1万
IP_DAY30_COUNT_1W_SECRET = 'k9mg3vbcr18hbf8'  # 优质-30天-上限1万

# IP_NO_GOLD_COUNT = '20231022638573801929'  # 按量计费套餐(金币模式)
# IP_NO_GOLD_COUNT_SECRET = 'u75guaahnuhk21o'  # 按量计费套餐(金币模式) - secret
IP_NO_GOLD_COUNT = '20240321523111871151'  # 按量计费套餐(金币模式)
IP_NO_GOLD_COUNT_SECRET = 'rvqghrkbkuel3g8'  # 按量计费套餐(金币模式) - secret


class PinzanHttpProxy(object):
    def __init__(self, area_code):
        self.sessions = global_session
        self.area_code = area_code
        self.proxy_meta = ''  # http://账号+密码+ip+端口
        self.host_port_city = '无'  # 端口和城市
        self.expired = 0  # 过期时间
        self.proxies = {}  # 代理对象

    def choices_time_ip(self):  # 选择时长ip
        # url = 'https://service.ipzan.com/userProduct-get?no=' + IP_DAY30_COUNT_6W_NO + '&userId=T0B8F090JPO'
        url = 'https://service.ipzan.com/userProduct-get?no=' + IP_DAY30_COUNT_6W_NO + '&userId=DVIJ01T1B28'
        response = self.sessions.get(url, impersonate="chrome101")
        status_code = response.status_code
        res = False
        if status_code == 200:
            json_data = response.json()
            if json_data.get('code') == 0:
                cur_data = json_data.get('data')
                limitDaySurplus = cur_data.get('limitDaySurplus')
                if limitDaySurplus and limitDaySurplus > 0:
                    res = True
        return res

    def get_proxy(self):
        self.proxies = {}
        self.host_port_city = '无'  # 置为空
        self.expired = 0  # 置为0
        self.proxy_meta = ''
        cur_session = self.sessions
        # 使用优质-30天-上限1万套餐，个数默认为1，默分钟数为5
        if self.choices_time_ip():
            # print('获取固定量的ip地址')
            no = IP_DAY30_COUNT_6W_NO  # 如果包时的可以用，则用包时的ip
            secret = IP_DAY30_COUNT_6W_SECRET
        else:
            # print('获取次数的ip地址')
            # no = '20231022638573801929'  # 否则用次数的ip
            no = IP_NO_GOLD_COUNT  # 否则用次数的ip
            secret = IP_NO_GOLD_COUNT_SECRET
        if self.area_code:
            url = ("https://service.ipzan.com/core-extract?"
                   "num=1"
                   "&no=" + no +
                   "&minute=1"
                   "&format=json"
                   "&area=" + str(self.area_code) +
                   "&repeat=1"
                   "&protocol=1"
                   "&pool=quality"
                   "&mode=auth"
                   "&secret=" + secret +
                   "&city=1")
        else:
            url = ("https://service.ipzan.com/core-extract?"
                   "num=1"
                   "&no=" + no +
                   "&minute=1"
                   "&format=json"
                   "&repeat=1"
                   "&protocol=1"
                   "&pool=quality"
                   "&mode=auth"
                   "&secret=" + secret +
                   "&city=1")
        # print('请求的url', url)
        response = cur_session.get(url, impersonate="chrome101")
        status_code = response.status_code
        if status_code == 200:
            # print('请求代理成功')
            json_data = response.json()
            if json_data.get('code') == 0:
                # print('请求代理成功-2')
                datas = json_data.get('data')
                if not datas:
                    # 报错，获取ip失败，获取成功，但是ip不存在
                    # print('ip不存在，获取失败')
                    pass
                data_list = datas.get('list')  # 默认获取一个ip
                if not data_list:
                    # 报错，获取ip失败，获取成功，但是ip不存在
                    # print('ip不存在，获取失败-2')
                    pass
                cur_ip_obj = data_list[0]
                host = cur_ip_obj.get('ip')
                port = cur_ip_obj.get('port')
                city = cur_ip_obj.get('city')
                host_port_city = str(host) + ':' + str(port) + city
                expired = cur_ip_obj.get('expired')  # 过期时间
                # net = cur_ip_obj.get('net')  # 移动/联通/电信
                account = cur_ip_obj.get('account')
                password = cur_ip_obj.get('password')
                # 账号密码验证
                proxy_meta = "http://%(account)s:%(password)s@%(host)s:%(port)s" % {
                    "account": account,
                    "password": password,
                    "host": host,
                    "port": port,
                }
                self.proxy_meta = proxy_meta
                self.host_port_city = host_port_city
                self.expired = expired
                proxies = {"http": proxy_meta, "https": proxy_meta}
                self.proxies = proxies
        # 返回代理对象和ip地址
        return
