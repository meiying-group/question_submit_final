import random

Iphone5_1MicroMessenger4_3_2 = "Mozilla/5.0 (iPhone; CPU iPhone OS 5_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Mobile/9B176 MicroMessenger/4.3.2 NetType/WIFI"
Android2_3_6MicroMessenger4_5_255 = "Mozilla/5.0 (Linux; U; Android 2.3.6; zh-cn; GT-S5660 Build/GINGERBREAD) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1 MicroMessenger/4.5.255 NetType/WIFI"
Android4_4_4MicroMessenger6_0_0_54 = "Mozilla/5.0 (Linux; Android 4.4.4; HM NOTE 1LTEW Build/KTU84P) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/33.0.0.0 Mobile Safari/537.36 MicroMessenger/6.0.0.54_r849063.501 NetType/WIFI"
Iphone6_1_3MicroMessenger5_0_1 = "Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_3 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10B329 MicroMessenger/5.0.1 NetType/WIFI"
Iphone12_1_4MicroMessenger7_0_3 = "Mozilla/5.0 (iPhone; CPU iPhone OS 12_1_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/16D57 MicroMessenger/7.0.3(0x17000320) NetType/WIFI Language/zh_CN"
IPad12_1_4MicroMessenger7_0_3 = "Mozilla/5.0 (iPad; CPU OS 12_1_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/16D57 MicroMessenger/7.0.3(0x17000320) NetType/WIFI Language/zh_CN"
Iphone12_1_4MicroMessenger6_7_4 = "Mozilla/5.0 (iPhone; CPU iPhone OS 12_1_4 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) Mobile/16D57 MicroMessenger/6.7.4 NetType/WIFI Language/zh_CN"
IPad12_1_4MicroMessenger6_7_4 = "Mozilla/5.0 (iPad; CPU OS 12_1_4 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) Mobile/16D57 MicroMessenger/6.7.4 NetType/WIFI Language/zh_CN"
Iphone8_0MicroMessenger6_0 = "Mozilla/5.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X) AppleWebKit/600.1.4(KHTML, like Gecko) Mobile/12A365 MicroMessenger/6.0 NetType/WIFI"


class UserAgentGenerate(object):

    @staticmethod
    def random_phone_micro_messenger():
        uas = [Iphone5_1MicroMessenger4_3_2, Android2_3_6MicroMessenger4_5_255, Android4_4_4MicroMessenger6_0_0_54,
               Iphone6_1_3MicroMessenger5_0_1, Iphone12_1_4MicroMessenger7_0_3, IPad12_1_4MicroMessenger7_0_3,
               Iphone12_1_4MicroMessenger6_7_4, IPad12_1_4MicroMessenger6_7_4, Iphone8_0MicroMessenger6_0]
        res = random.choice(uas)
        return res

    @staticmethod
    def random_pc():
        res = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36'
        return res
