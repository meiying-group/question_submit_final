import json
from dataclasses import dataclass, asdict, field
from json import dumps
from flask import jsonify

SUCCESS = 200
SUCCESS_MESSAGE = "请求成功"


@dataclass
class CommomResponse:
    code: int
    message: str
    result: object

    @property
    def __dict__(self):
        return asdict(self)

    # @property
    # def json(self):
    #     return dumps(self.__dict__)


def success(res):
    return CommomResponse(code=200, message="请求成功", result=res)


def success_message(res, message):
    return CommomResponse(code=200, message=message, result=res)


def failed_10001(res):
    return CommomResponse(code=400, message="请求参数错误", result=res)


def success_response(res: object):
    obj = CommomResponse(code=200, message="请求成功", result=res).__dict__
    return jsonify(obj)


def failed_response(res: object):
    return jsonify(CommomResponse(code=500, message="请求失败", result=res).__dict__)
