from pydantic import BaseModel
from typing import List, Any


# 定义模型类
# @dataclass
class SendResults(BaseModel):
    work_order_id: int  # 工单id
    item_answer_start: int  # 一行数据的答题开始时间 60 - 10
    item_answer_end: int  # 一行数据的答题结束时间 120 - 10
    deal_time_min: int  # 单次答题-最少答题个数 8 - 10
    deal_time_max: int  # 单次答题-最多答题个数 12 - 10
    interval_start: int  # 最小间隔答题时间 100 - 30
    interval_end: int  # 最大间隔答题时间 200 - 30
    can_answer_start: int  # 可以答题的开始时间
    can_answer_end: int  # 可以答题的结束时间
    submit_type: str  # 提交类型
    is_base_order: bool  # 是否乱序提交
    regionFormValues: list  # 是否乱序提交


class WorkOrderIdModel(BaseModel):
    work_order_id: int  # 工单id


class UserView(BaseModel):
    """
    用户登录的view对象
    """
    user_name: str
    password: str


class RegisterView(BaseModel):
    """
    用户注册的view对象
    """
    user_name_zh: str
    user_name: str
    password: str


class RelationView(object):
    """
    相关性参数
    """
    work_order_id: int
    correlation: Any
    do_ra: bool  # 信度
    do_va: bool  # 效度
    do_ca: bool  # 相关
    do_fa: bool  # 频数
    do_re: bool  # 回归
    re_v: Any  # 回归的因变量
    re_dv: Any  # 回归的自变量
    relation_generate_type: str  # 正态或者偏态分布
